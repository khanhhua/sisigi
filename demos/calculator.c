#include "../src/sisigi.h"
#include <stdbool.h>
#include <stdio.h>

bool is_equal(char *str1, char *str2) {
  int i = 0;

  while (str1[i] == str2[i]) {
    if (str1[i] == '\0') {
      return true;
    }

    i++;
  }

  return false;
}

int main() {
  Request *req = read_request();
  char *num1, *num2, *op;
  num1 = get_param(req, "num1");
  num2 = get_param(req, "num2");
  op = get_param(req, "op");

  int a, b, c;

  sscanf(num1, "%d", &a);
  sscanf(num2, "%d", &b);

  if (is_equal(op, "plus")) {
    c = a + b;
    printf("%d\n", c);
  } else if (is_equal(op, "minus")) {
    c = a - b;
    printf("%d\n", c);
  } else if (is_equal(op, "multiply")) {
    c = a * b;
    printf("%d\n", c);
  } else if (is_equal(op, "divide")) {
    c = a / b;
    printf("%d\n", c);
  }

  return 0;
}
