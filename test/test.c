#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../src/base64.h"
#include "../src/cgi.h"
#include "../src/headerlist.h"
#include "../src/namedlist.h"
#include "../src/parsers.h"
#include "../src/scanners.h"
#include "../src/sisigi.h"
#include "assertions.h"

const char *url_root = "/";
const char *url_pages = "/pages";
const char *url_page_1 = "/pages/1";

const char *url_encoded_query = "name=tom&message=ich%20bin%20schuldig";

void test_pattern_scanf_total_match() {
  char *line1 = "Content-Disposition: form-data; name=\"address\"";
  char *line2 = "Content-Disposition: form-data; name=\"email\"";
  char *name;
  char *rest;

  bool result = pattern_scanf(
      line1, "Content-Disposition: form-data; name=\"%s\"", &name, &rest);

  assert(result);
  equal_s(name, "address");
  equal_null(rest);

  result = pattern_scanf(line2, "Content-Disposition: form-data; name=\"%s\"",
                         &name, &rest);

  assert(result);
  equal_s(name, "email");
  equal_null(rest);
}

void test_pattern_scanf_two_matches() {
  char *source = "Content-Disposition: form-data; name=\"avatar\"; "
                 "filename=\"avatar.png\"";
  char *name;
  char *rest;

  bool result;

  result = pattern_scanf(
      source, "Content-Disposition: form-data; name=\"%s\"; ", &name, &rest);
  assert(result);
  equal_s(name, "avatar");

  result = pattern_scanf(rest, "filename=\"%s\"", &name, &rest);
  assert(result);
  equal_s(name, "avatar.png");

  equal_null(rest);
}

void test_get_method() {
  Request req = {"GET", "/", NULL, NULL};

  char *method = get_method(&req);
  equal_s(method, "GET");
}

void test_get_content_type() {
  HeaderList *headers = create_header_list("Content-Type", "text/plain");
  Request req = {"GET", NULL, headers, NULL};

  char *content_type = get_content_type(&req);
  equal_s(content_type, "text/plain");
}

void test_add_header() {
  HeaderList *headers = create_header_list("Content-Type", "text/plain");
  add_header_item(headers, "Accept", "text/html");
  Request req = {"GET", "/", headers, NULL};

  equal_s(get_header(&req, "Content-Type"), "text/plain");
  equal_s(get_header(&req, "Accept"), "text/html");
}

void test_read_get_request() {
  setenv(REQUEST_METHOD, "GET", 0);
  setenv(PATH_INFO, "/", 0);
  setenv(SERVER_NAME, "localhost:8080", 0);
  setenv(CONTENT_LENGTH, "0", 0);

  Request *req = read_request();
  equal_s(req->method, "GET");
  equal_s(req->path, "/");
  equal_s(get_header(req, "Host"), "localhost:8080");

  unsetenv(REQUEST_METHOD);
  unsetenv(PATH_INFO);
  unsetenv(SERVER_NAME);
  unsetenv(CONTENT_LENGTH);
}

void test_read_post_request() {
  setenv(REQUEST_METHOD, "POST", 0);
  setenv(PATH_INFO, "/api", 0);
  setenv(SERVER_NAME, "localhost:8080", 0);
  setenv(CONTENT_TYPE, "application/x-www-form-urlencoded", 0);
  setenv(CONTENT_LENGTH, "33", 0);

  freopen("./test/fixtures/form-data.txt", "r", stdin);

  Request *req = read_request();
  equal_s(req->method, "POST");
  equal_s(req->path, "/api");
  equal_s(get_header(req, "Host"), "localhost:8080");
  equal_s(req->body, "email=admin@example.com&fname=tom");

  equal_s(get_param(req, "email"), "admin@example.com");
  equal_s(get_param(req, "fname"), "tom");

  unsetenv(REQUEST_METHOD);
  unsetenv(PATH_INFO);
  unsetenv(SERVER_NAME);
  unsetenv(CONTENT_TYPE);
  unsetenv(CONTENT_LENGTH);

  fclose(stdin);
}

void test_parse_urlencoded_data() {
  char *urlencoded = "email=admin@example.com&fname=tom";
  NamedList *items = parse_urlencoded(urlencoded);
  BufferInfo buffer;
  int type;

  get_value(items, "email", &type, &buffer);
  equal_s(buffer.data, "admin@example.com");
  equal_i(type, TYPE_STORAGE_PLAIN_TEXT);

  get_value(items, "fname", &type, &buffer);
  equal_s(buffer.data, "tom");
  equal_i(type, TYPE_STORAGE_PLAIN_TEXT);
}

void test_parse_multipart_form_data() {
  setenv(REQUEST_METHOD, "POST", 0);
  setenv(PATH_INFO, "/api", 0);
  setenv(SERVER_NAME, "localhost:8080", 0);
  setenv(CONTENT_TYPE, "multipart/form-data; boundary=\"0htheboundary1\"", 0);
  setenv(CONTENT_LENGTH, "155", 0);

  freopen("./test/fixtures/multipart-01.txt", "r", stdin);

  Request *req = read_request();
  equal_s(req->method, "POST");
  equal_s(req->path, "/api");
  equal_s(get_header(req, "Host"), "localhost:8080");

  equal_s(get_param(req, "email"), "admin@gmail.com");
  equal_s(get_param(req, "name"), "tom");

  unsetenv(REQUEST_METHOD);
  unsetenv(PATH_INFO);
  unsetenv(SERVER_NAME);
  unsetenv(CONTENT_TYPE);
  unsetenv(CONTENT_LENGTH);

  fclose(stdin);
}

void test_parse_multipart_form_data_file_upload() {
  setenv(REQUEST_METHOD, "POST", 0);
  setenv(PATH_INFO, "/api", 0);
  setenv(SERVER_NAME, "localhost:8080", 0);
  setenv(CONTENT_TYPE, "multipart/form-data; boundary=\"0htheboundary1\"", 0);
  setenv(CONTENT_LENGTH, "3303", 0);

  FILE *uploaded = fopen("./tmp/tempofile", "w");
  freopen("./test/fixtures/multipart-02.txt", "r", stdin);

  Request *req = read_request();
  equal_s(req->method, "POST");
  equal_s(req->path, "/api");
  equal_s(get_header(req, "Host"), "localhost:8080");

  BufferInfo buffer;
  get_file(req, "avatar", &buffer);
  fwrite(buffer.data, sizeof(char), buffer.size, uploaded);
  fclose(uploaded);

  equal_file("./tmp/tempofile", "./test/fixtures/mario.gif");
  equal_s(get_param(req, "exif"), "{\"Manufacturer\":\"Canon\"}");

  unsetenv(REQUEST_METHOD);
  unsetenv(PATH_INFO);
  unsetenv(SERVER_NAME);
  unsetenv(CONTENT_TYPE);
  unsetenv(CONTENT_LENGTH);

  fclose(stdin);
}

void test_render_simple_plaintext_response() {
  // credit
  // https://stackoverflow.com/questions/11042218/c-restore-stdout-to-terminal#11042581
  int saved_stdout = dup(1);
  freopen("./tmp/out.txt", "w", stdout);

  Response *res = create_response(200);
  set_response_header(res, "Content-Type", "text/plain");
  set_response_body(res, "Hello World");
  send_response(res);

  fclose(stdout);
  dup2(saved_stdout, 1);

  equal_i(res->status, 200);
  equal_s(get_header_item(res->headers, "Content-Type"), "text/plain");
  equal_s(get_header_item(res->headers, "Content-Length"), "11");

  equal_file("./tmp/out.txt", "./test/fixtures/simple-response.txt");
}

void test_base64_decode() {
  equal_s(base64_decode("TWFu"), "Man");
  equal_s(base64_decode("QXRvbWlj"), "Atomic");
  equal_s(base64_decode("IUAjJCVeJiooKQ=="), "!@#$%^&*()");
  equal_s(base64_decode("fg=="), "~");
  equal_s(base64_decode("fn4="), "~~");
  equal_s(base64_decode("fn5+"), "~~~");
  equal_s(base64_decode("fn5+fn5+"), "~~~~~~");
}

void test_base64_decode_file() {
  FILE *file = fopen("./test/fixtures/mario.gif", "r");
  char *expected = (char *)calloc(2269, sizeof(char));
  fread(expected, sizeof(char), 2269, file);
  fclose(file);

  file = fopen("./test/fixtures/mario.gif.b64", "r");
  char *actual_raw = (char *)calloc(3029, sizeof(char));
  fread(actual_raw, sizeof(char), 3029, file);
  fclose(file);

  char *actual = base64_decode(actual_raw);

  for (int i = 0; i < 2269; i++) {
    equal_i(actual[i], expected[i]);
  }

  file = fopen("./tmp/tempofile-1.gif", "w");
  fwrite(actual, sizeof(char), 2269, file);
  fclose(file);
}

int main() {
  test_base64_decode();

  test_base64_decode_file();

  test_parse_multipart_form_data_file_upload();
  test_parse_urlencoded_data();
  test_parse_multipart_form_data();

  test_pattern_scanf_total_match();
  test_pattern_scanf_two_matches();

  test_get_method();
  test_get_content_type();
  test_add_header();

  test_read_get_request();
  test_read_post_request();

  test_render_simple_plaintext_response();

  return 0;
}
