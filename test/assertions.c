// IMPLEMENTATION of assertions.h
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 1024

char *readfile(char *, int);

void equal_s(char *actual, char *expected) {
  assert(strlen(actual) == strlen(expected));
  assert(0 == strcmp(actual, expected));
}

void equal_i(int actual, int expected) { assert(actual == expected); }

void same(void *actual_p, void *expected_p) { assert(actual_p == expected_p); }

void equal_null(void *actual) { assert(actual == NULL); }

void equal_file(char *actual_path, char *expected_path) {
  char *actual = readfile(actual_path, BUFFER_SIZE);
  char *expected = readfile(expected_path, BUFFER_SIZE);

  equal_s(actual, expected);
  free(actual);
  free(expected);
}

char *readfile(char *path, int size) {
  FILE *fd = fopen(path, "r");
  char *content = (char *)calloc(size, sizeof(char));
  fgets(content, size, fd);

  fclose(fd);
  fd = NULL;

  return content;
}
