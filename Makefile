SOURCE := request.c response.c sisigi.c namedlist.c scanners.c parsers.c headerlist.c base64.c

clean:
	rm -Rf ./bin/* ./obj/*

%.c:
	@echo "Compiling C file..."
	@gcc -Wall -std=gnu99 -c -fpic ./src/$@ -o ./obj/$(basename $@).o -lm

libsisigi: $(SOURCE) 
	@gcc -Wall -std=gnu99 -shared -fpic $(wildcard ./obj/*.o) \
			-o ./obj/libsisigi.so -lm

test_assertions:
	@gcc -Wall -std=gnu99 -c -fpic ./test/assertions.c -o ./obj/libassertions.so

test: test_assertions libsisigi
	@gcc -Wall -std=gnu99 -L./obj -o ./bin/test -lassertions $(wildcard ./obj/*.o) test/test.c -lm
	./bin/test


.PHONY: libsisigi test clean
