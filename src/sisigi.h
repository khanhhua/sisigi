#include "headerlist.h"
#include "namedlist.h"

typedef struct {
  char *method;
  char *path;
  HeaderList *headers;
  char *body;
  NamedList *params;
} Request;

typedef struct {
  int status;
  HeaderList *headers;
  char *body;
} Response;

/**
 * Extract part of URL according to a pattern
 */
void scan_url(const char *, char *, char *);

/**
 * Read and construct the request
 */
Request *read_request();

char *get_method(Request *);

char *get_content_type(Request *);

char *get_header(Request *, char *);

char *get_param(Request *, char *);

bool get_file(Request *, char *, BufferInfo *);

Response *create_response(int);
/**
 * Build a response
 */

void set_response_status(Response *, int);

void set_response_header(Response *, char *, char *);

void set_response_body(Response *, char *);
/**
 * Write response back to downstream server
 */
void send_response(Response *);
