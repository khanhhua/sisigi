#pragma once
#include "namedlist.h"

typedef struct HeaderList {
  NamedList *items;
} HeaderList;

HeaderList *create_header_list(char*, char*);

void add_header_item(HeaderList*, char*, char*);

char *get_header_item(HeaderList*, char*);
