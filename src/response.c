#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "namedlist.h"
#include "sisigi.h"

#define HTTP_200 "OK"
#define HTTP_201 "Created"
#define HTTP_400 "Bad Request"
#define HTTP_401 "Unauthorized"
#define HTTP_402 "Payment Required"
#define HTTP_403 "Forbidden"
#define HTTP_404 "Not Found"
#define HTTP_500 "Internal Server Error"

char *http_code_to_reason(int);
void write_header(char**, Response*, char*);

Response *create_response(int status) {
  Response *res = (Response*)calloc(1, sizeof(Response)); 
  res->status = status;

  return res;
}

void set_response_status(Response *res, int status) {
  res->status = status;
}

void set_response_header(Response* res, char* name, char* value) {
  if (res->headers == NULL) {
    res->headers = create_header_list(name, value);
    return;
  }

  add_header_item(res->headers, name, value);
}

void set_response_body(Response* res, char* body) {
  int content_length = strlen(body);
  
  res->body = body;
  if (content_length) {
    char *content_length_s;
    asprintf(&content_length_s, "%d", content_length);
    set_response_header(res, "Content-Length", content_length_s);
  }
}
/**
 * Write response back to downstream server
 */
void send_response(Response *res) {
  char *out;
  asprintf(&out, "Status: %d %s\r\n", res->status, http_code_to_reason(res->status));

  write_header(&out, res, "Content-Type");

  if (res->body) {
    char* line;
    asprintf(&line, "\r\n%s\r\n", res->body);
    strcat(out, line);
  }

  printf("%s\r\n", out);
}

char *http_code_to_reason(int code) {
  switch (code) {
    case 200: return HTTP_200;
    case 201: return HTTP_201;
    case 400: return HTTP_400;
    case 401: return HTTP_401;
    case 402: return HTTP_402;
    case 403: return HTTP_403;
    case 404: return HTTP_404;
    default: return HTTP_500;
  }
}

void write_header(char **out, Response *res, char *header_name) {
  char *header_value = get_header_item(res->headers, header_name);

  if (!header_value) {
    return;
  }

  char *line;
  asprintf(&line, "%s: %s\r\n", header_name, header_value);
  strcat(*out, line);
  //free(line);
}
