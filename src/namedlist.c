#define _GNU_SOURCE

#include "namedlist.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

NamedList *get_item(NamedList *, char *);

NamedList *namedlist(int type, unsigned short size, char *name, char *value) {
  NamedList *list = (NamedList *)calloc(1, sizeof(NamedList));
  NamedValue head = {type, size, name, value};
  list->head = head;

  return list;
}

NamedList *add_item(NamedList *list, int type, unsigned short size, char *name,
                    char *value) {
  NamedList *head = namedlist(type, size, name, value);
  head->next = list;

  return head;
}

bool get_value(NamedList *list, char *name, int *type, BufferInfo *buffer) {
  NamedList *head = get_item(list, name);

  if (head == NULL) {
    return false;
  } else {
    if (type != NULL) {
      *type = head->head.type;
    }

    buffer->data = head->head.value;
    buffer->size = head->head.size;

    return true;
  }
}

NamedList *set_value(NamedList *list, int type, unsigned short size, char *name,
                     char *value) {
  NamedList *head = get_item(list, name);

  if (head == NULL) {
    return add_item(list, type, size, name, value);
  }

  head->head.value = value;
  head->head.size = size;

  return list;
}

NamedList *get_item(NamedList *list, char *name) {
  NamedValue head = list->head;

  if (strcmp(head.name, name) == 0) {
    return list;
  } else if (list->next) {
    return get_item(list->next, name);
  } else {
    return NULL;
  }
}
