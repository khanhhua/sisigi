#define _GNU_SOURCE

#include "base64.h"
#include "namedlist.h"
#include "scanners.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CRLF "\r\n"
#define CRLF_SIZE 2

NamedList *parse_urlencoded(char *raw) {
  NamedList *items;

  char *source = (char *)calloc(strlen(raw), sizeof(char));
  strcpy(source, raw);

  while (source != NULL) {
    char *pair_s = strsep(&source, "&");

    char *name = strsep(&pair_s, "=");
    char *value = pair_s;
    unsigned short size = strlen(value);

    if (items == NULL) {
      items = namedlist(TYPE_STORAGE_PLAIN_TEXT, size, name, value);
    } else {
      items = add_item(items, TYPE_STORAGE_PLAIN_TEXT, size, name, value);
    }
  }

  return items;
}

char *readline(char **ptr, char *eof) {
  if (*ptr == NULL) {
    return NULL;
  }

  int length = 0;
  char *line;
  char *crlf = strstr(*ptr, CRLF);

  if (crlf == NULL) {
    return NULL; // WARN: Line to read is not CRLF terminated
  }

  length = crlf - (*ptr);
  line = (char *)calloc(
      length + 1,
      sizeof(char)); // 1 for the \0 terminator at the end of sz strings
  if (length > 0) {
    strncpy(line, *ptr, length); // Paste the line here
  }

  if (crlf + CRLF_SIZE >= eof) {
    *ptr = NULL;
  } else {
    *ptr = crlf + CRLF_SIZE;
  }

  return line;
}

bool equals(char *str1, char *str2) { return strcmp(str1, str2) == 0; }

NamedList *parse_multipart(char *raw, char *boundary) {
  if (raw == NULL) {
    return NULL;
  }

  int length = strlen(raw);
  char *ptr = raw;
  char *eof = raw + length;

  bool expecting_name = false, expecting_value = false;
  char *name, *type = NULL, *transfer_encoding = NULL, *value;
  char *line = readline(&ptr, eof);

  NamedList *items = NULL;

  do {
    // printf("##PARSERS.c## line = >>%s<<  #loopstart\n", line);
    char *rest;

    if (equals(line, boundary)) {
      expecting_name = true;
    } else if (expecting_name &&
               pattern_scanf(line,
                             "Content-Disposition: form-data; name=\"%s\"",
                             &name, &rest)) {
      char *saved_ptr = ptr;
      line = readline(&ptr, eof);
      int buffer_size = strlen(line) - strlen("Content-Type: ");

      type = (char *)calloc(buffer_size, sizeof(char));

      if (-1 != sscanf(line, "Content-Type: %s", type)) {
        line = readline(&ptr, eof);

        buffer_size = strlen(line) - strlen("Content-Transfer-Encoding: ");
        transfer_encoding = (char *)calloc(buffer_size, sizeof(char));
        if (-1 !=
            sscanf(line, "Content-Transfer-Encoding: %s", transfer_encoding)) {
        } else {
          ptr = saved_ptr;
        }
      } else {
        ptr = saved_ptr;
      }
      expecting_name = false;
    } else if (equals(line, "")) {
      expecting_value = true;
    } else if (expecting_value) {
      int storage_type;
      unsigned short size;

      if (type == NULL) {
        storage_type = TYPE_STORAGE_PLAIN_TEXT;
        size = strlen(line);
        value = (char *)calloc(size + 1, sizeof(char));
        strncpy(value, line, size);
      } else {
        if (transfer_encoding && strcmp("base64", transfer_encoding) == 0) {
          storage_type = TYPE_STORAGE_BINARY;
          value = base64_decode(line);
          size = strlen(line) * 3.0 / 4.0;
        } else {
          storage_type = TYPE_STORAGE_PLAIN_TEXT;
          size = strlen(line);
          value = (char *)calloc(size + 1, sizeof(char));
          strncpy(value, line, size);
        }
      }

      if (items == NULL) {
        items = namedlist(storage_type, size, name, value);
      } else {
        items = add_item(items, storage_type, size, name, value);
      }

      expecting_value = false;
    }

    line = readline(&ptr, eof);
  } while (line != NULL);

  return items;
}
/*
NamedList *parse_multipart(char *raw, char *boundary) {
  if (raw == NULL) {
    return NULL;
  }

  int length = strlen(raw);

  NamedList *items = NULL;
  char *ptr =
      raw; // This pointer will traverse the contigious raw data allocation.
  char *eof = raw + length;
  char *crlf;
  char *line = NULL;
  char *name = NULL;
  char *value = NULL;
  bool ready = false;

  do {
    crlf = strstr(ptr, CRLF);
    if (crlf == ptr) {  // Empty line?
      ptr += CRLF_SIZE; // Move the reading head 2 chars forward
      // protect against memory overrun attacks of all shapes and length
      if (ptr >= eof) {
        break;
      } else {
        continue;
      }
    }

    line = (char *)calloc(
        crlf - ptr + CRLF_SIZE,
        sizeof(char)); // 1 for the \0 terminator at the end of sz strings
    strncpy(line, ptr, crlf - ptr); // Paste the line here

    if (strcmp(line, boundary) ==
        0) { // should I write if(!strcmp(line, boundary)) {}???
      free(line);
      name = NULL;
      value = NULL;

      ready = true;
    } else if (ready && !name) {
      pattern_scanf(line, "Content-Disposition: form-data; name=\"%s\"", &name);
    } else if (ready && name && !value) {
      value = (char *)calloc(strlen(line), sizeof(char));
      strcpy(value, line);

      if (items == NULL) {
        items = namedlist(name, value);
      } else {
        items = add_item(items, name, value);
      }

      ready = false;
    }

    ptr = (crlf + CRLF_SIZE);
  } while (ptr < eof);

  free(line);
  return items;
}*/
