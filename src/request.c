#define _GNU_SOURCE
#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cgi.h"
#include "headerlist.h"
#include "namedlist.h"
#include "parsers.h"
#include "scanners.h"
#include "sisigi.h"

#define MAX_BODY_LENGTH 32767

typedef struct {
  char *st;
  char *nd;
} Pair;

Pair cgi_header_pairs[] = {{"Content-Length", CONTENT_LENGTH},
                           {"Content-Type", CONTENT_TYPE},
                           {"Host", SERVER_NAME}};

int PAIR_COUNT = sizeof(cgi_header_pairs) / sizeof(cgi_header_pairs[0]);

void collect_cgi_headers(Request *);

void read_body(Request *);

Request *read_request() {
  Request *req = (Request *)calloc(1, sizeof(Request));
  unsigned int length = 0;

  req->method = getenv(REQUEST_METHOD);
  req->path = getenv(PATH_INFO);
  collect_cgi_headers(req);

  sscanf(getenv(CONTENT_LENGTH), "%u", &length);
  if (length > 0) {
    char *body =
        (char *)calloc(length, sizeof(char)); // BufferInfo overrun har har har
    fread(body, sizeof(char), length, stdin);

    req->body = body;
  } else {
    req->body = NULL;
  }

  return req;
}

char *get_method(Request *req) { return req->method; }

char *get_content_type(Request *req) { return get_header(req, "Content-Type"); }

char *get_header(Request *req, char *name) {
  return get_header_item(req->headers, name);
}

char *get_param(Request *req, char *name) {
  read_body(req);

  if (req->params) {
    BufferInfo buffer;

    if (get_value(req->params, name, NULL, &buffer)) {
	  return buffer.data;
    }

    return NULL;
  }

  return get_header(req, name);
}

bool get_file(Request *req, char *name, BufferInfo *buffer) {
  read_body(req);

  int type = 0;

  if (get_value(req->params, name, &type, buffer)) {
    if (type == TYPE_STORAGE_BINARY) {
      return true;
    }
  }

  printf("Could not read param %s\n", name);
  return false;
}

void read_body(Request *req) {
  if (req->body == NULL) {
    printf("WARN: Body is null\n");
    return;
  }

  if (req->params) {
    return;
  }

  NamedList *params = NULL;
  char *content_type = get_content_type(req);

  if (0 == strcmp(content_type, "application/x-www-form-urlencoded")) {
    params = parse_urlencoded(req->body);
    req->params = params;
  } else {
    char *boundary;
    char *rest;

    if (pattern_scanf(content_type, "multipart/form-data; boundary=\"%s\"",
                      &boundary, &rest)) {
      char *prefixed = (char *)calloc(strlen(boundary) + 3, sizeof(char));
      sprintf(prefixed, "--%s", boundary);
      params = parse_multipart(req->body, prefixed);

      free(prefixed);
      req->params = params;
    }
  }

  assert(params != NULL);
}

void collect_cgi_headers(Request *req) {
  HeaderList *hlist = NULL;
  Pair *p = &cgi_header_pairs[0];

  hlist = create_header_list(p->st, getenv(p->nd));
  for (int i = 1; i < PAIR_COUNT; i++) {
    p = cgi_header_pairs + i;
    char *value = getenv(p->nd);

    if (value == NULL) {
      continue;
    }

    add_header_item(hlist, p->st, getenv(p->nd));
  }

  req->headers = hlist;
}
