#define _GNU_SOURCE

#include "headerlist.h"
#include "namedlist.h"
#include <stdio.h>

#include <stdlib.h>
#include <string.h>

HeaderList *create_header_list(char *name, char *value) {
  HeaderList *hlist = (HeaderList *)calloc(1, sizeof(HeaderList));
  unsigned short size = strlen(value);
  NamedList *items = namedlist(TYPE_STORAGE_PLAIN_TEXT, size, name, value);
  hlist->items = items;
  return hlist;
}

void add_header_item(HeaderList *hlist, char *name, char *value) {
  unsigned short size = strlen(value);
  hlist->items =
      add_item(hlist->items, TYPE_STORAGE_PLAIN_TEXT, size, name, value);
}

char *get_header_item(HeaderList *hlist, char *name) {
  BufferInfo buffer;
  if (get_value(hlist->items, name, NULL, &buffer)) {
    return buffer.data;
  }

  return NULL;
}
