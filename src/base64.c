#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ANSI_NUM_0 48
#define ANSI_NUM_9 57
#define ANSI_BIG_A 65
#define ANSI_BIG_Z 90
#define ANSI_SMALL_A 97
#define ANSI_SMALL_Z 122
#define ANSI_SLASH 47
#define ANSI_PLUS 43
// Padding
#define ANSI_EQUAL 61

int div_up(int x, int y) { return (x + y - 1) / y; }

int div_down(int x, int y) { return x / y; }

int to_oct(char c) {
  if (c == ANSI_PLUS) {
    return 0b111110;
  } else if (c == ANSI_SLASH) {
    return 0b111111;
  } else if (c >= ANSI_NUM_0 && c <= ANSI_NUM_9) {
    return 0b110100 + (c - ANSI_NUM_0);
  } else if (c >= ANSI_BIG_A && c <= ANSI_BIG_Z) {
    return (c - ANSI_BIG_A);
  } else if (c >= ANSI_SMALL_A && c <= ANSI_SMALL_Z) {
    return 0b011010 + (c - ANSI_SMALL_A);
  }

  return 0;
}

char *itoa(int b) {
  int length;

  if (b == 0) {
    length = 0;
  } else {
    length = log2(b);
  }

  char *buffer = (char *)calloc(length, sizeof(char));
  int i = length;
  while (i >= 0) {
    if (b & 1) {
      buffer[i] = '1';
    } else {
      buffer[i] = '0';
    }
    b = b >> 1;
    i--;
  }

  return buffer;
}

char *base64_decode(char *encoded) {
  if (encoded == NULL) {
    return NULL;
  }

  int length = strlen(encoded);
  if (length < 4) {
    return NULL;
  }

  unsigned int p = 0;
  int p0, p1, p2, p3;
  int buffer_size = div_down(length * 3, 4);
  char *buffer = (char *)calloc(buffer_size, sizeof(char));

  for (int i = 0, j = 0; i < length; i += 4, j += 3) {
    if (encoded[i] == ANSI_EQUAL) {
      break;
    }

    p0 = to_oct(encoded[i]);
    p1 = to_oct(encoded[i + 1]);
    p2 = to_oct(encoded[i + 2]);
    p3 = to_oct(encoded[i + 3]);

    p = p0 << 18 | p1 << 12 | p2 << 6 | p3;
    buffer[j] = p >> 16;
    buffer[j + 1] = p >> 8 & 0xFF;
    buffer[j + 2] = p & 0xFF;
  }

  return buffer;
}
