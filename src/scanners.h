#include <stdbool.h>

/**
 * Scan for a pattern in a given string, store the first match if any.
 * Pointer to the string beyond the match is the remainder.
 *
 * Returns true if a match is found
 */
bool pattern_scanf(char *, const char *, char **, char **);
