#define _GNU_SOURCE

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRING_QUOTE '"'

bool pattern_scanf(char *line, const char *pattern, char **out, char **rest) {
  char *p = strstr(pattern, "%s");
  assert((p - 1)[0] == STRING_QUOTE); // quote assertions
  assert((p + 2)[0] == STRING_QUOTE);

  if (p == NULL) {
    return false;
  }

  int offset = p - pattern;
  char *marker_start = NULL;
  char *marker_end = NULL;

  for (int i = 0; i < offset; i++) {
    if (*(line + i) != *(pattern + i)) {
      return false;
    }
  }
  marker_start = line + offset;

  // find the match beyond %s within "line"
  if (strlen(p + 2) == 0) {
    marker_end = marker_start + strlen(marker_start);
    *rest = NULL;
  } else {
    marker_end = strchr(marker_start, STRING_QUOTE);
  }

  if (marker_end == NULL) {
    return false;
  } else if (marker_start == marker_end) {
    return false; // Sorry but I don't scan empty strings
  }

  if (marker_end[1] == '\0') {
    *rest = NULL;
  } else {
    char *beyond = strstr(marker_end, (p + 3));
    if (beyond == NULL) {
      return false;
    }

    *rest = beyond + strlen(p + 3);
  }

  int match_length = marker_end - marker_start;
  *out = (char *)calloc(match_length + 1, sizeof(char));
  strncpy(*out, marker_start, match_length);
  return true;
}
