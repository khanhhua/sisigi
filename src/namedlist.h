#pragma once

#include <stdbool.h>

#define TYPE_STORAGE_PLAIN_TEXT 0
#define TYPE_STORAGE_BINARY 1
#define TYPE_STORAGE_EXTERNAL 2

typedef struct {
  int type;
  unsigned short size;
  char *name;
  char *value;
} NamedValue;

typedef struct NamedList {
  NamedValue head;
  struct NamedList *next;
} NamedList;

typedef struct {
  unsigned short size;
  char *data;
} BufferInfo;

NamedList *namedlist(int, unsigned short, char *, char *);

NamedList *add_item(NamedList *, int, unsigned short, char *, char *);

NamedList *set_value(NamedList *, int, unsigned short, char *, char *);

bool get_value(NamedList *, char *, int *, BufferInfo *);
